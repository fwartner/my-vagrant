my-vagrant
==========

Personal Vagrant-Setup fully configured

***

This is my personal setup of [vagrant](http://www.vagrantup.com/) made with [puppet](http://puppetlabs.com/).  
It includes all modules i´m using everyday for work.

The main system will setup on Ubuntu 12.04 (x64).

### Configuration
The configuration of this vm is pretty easy.  
You only have to change some variables in the `puphpet/config.yaml` file.

Please change the `port` & `folder` variables to make this vm working.  
Otherwise you will get an error in case the port is already used by the system or another vm.

### Run
To run this vm, all you have to do is typing one line in your console:

`vagrant up`

### Issues
Please open a new [issue](https://github.com/fwndev/my-vagrant/issues/new) at the repository page.